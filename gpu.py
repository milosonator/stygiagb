# python 34
import pygame

#from graphics import *
#from multiprocessing import Process

class Gpu:

    def __init__(self, memory):
        print("init GPU")
        # Create graphics window
        #self.win = GraphWin("Game Boy", 160, 144, autoflush=False)
        #self.win = GraphWin("Game Boy", 500, 500, autoflush=False)
        #self.w = sf.RenderWindow(sf.VideoMode(160, 144), "Stygia GB")
        pygame.init()
        self.screen = pygame.display.set_mode((160, 144), pygame.DOUBLEBUF)

        self.mem = memory
        self.vcycles = 0
        # create bg image, anchored at point 0,0 size 256 by 256
        self._bg = pygame.Surface((256, 256))
        self._bg.fill((175, 203, 70)) 
        self.bg_surf = pygame.Surface((256, 256))

        # invalid tiles
        self.invalid_tiles = []
        self.redraw = True
        
        #self.win.addItem(self.bg)

        #self.bg_data = bytearray(256*256*4)
        #self.bg_tex = sf.Texture.create(256, 256)
        
        # used for not spawing multiple drawing threads
        #self.drawing = False

        self.white      = (175, 203, 70 , 255)
        self.light_grey = (121, 170, 109, 255)
        self.dark_grey  = (34 , 111, 95 , 255)
        self.black      = (8  , 41 , 85 , 255)
       
        #self.clear_screen()

        #rec = Rectangle(Point(0,0), Point(256, 256))
        #rec.setFill(self.white)
        #rec.draw(self.win)

    def tick(self, c):
        self.vcycles += c
        for event in pygame.event.get():
            if event.type == pygame.QUIT: 
                self.mem.memdump()
                exit()


        # set LY:
        ly = int(self.vcycles / 456)
        if ly == 0x90:
            #start vblank period
            #draw stuff
            self.redraw = self.draw_background() or self.redraw
            #self.draw_sprites()
            

        self.mem.write_byte(0xFF44, ly)
        if self.vcycles >= 70220:
            print("!!!!!!!!!!redraw!!!!!!!!!!!")
            #self.p_end_draw_background()
            self.vcycles = 0
            print("updateing window")
            #self.win.redraw()
            #self.bg.save("scrn.png")
            #self.win.update()
            # merge layers
            # add bg
            if self.redraw:
                print("redrawing")
                redraw = False
                self.screen.blit(self._bg, (0,0))
                self.screen.blit(self.bg_surf, (0,0))
                pygame.display.flip()
                print(pygame.display.Info())

    #def p_end_draw_background(self):
        #print("waiting for drawing to be done time")
        #start = time.clock()
        #self.p.join()
        #self.drawing = False
        #elapsed = time.clock() - start
        #print("time spent in (function name) is: " + str(elapsed*1000))

    def draw_background(self):
        
        if not self.mem.video_memory_invalidated():
            # if no tiles need to be reset, just return
            self.mem.validate_video_memory()
            print("no redraw needed")
            return False
        
        # prepare pixel array
        self.bg_pix = pygame.PixelArray(self.bg_surf)

        # draw tiles into place
        for i in range(0x0000, 0x03ff):
            tile = self.mem.read_byte(0x9800 + i)
            self.draw_tile(tile, i)

        del self.bg_pix
        return True
        
    
    def draw_sprites(self):
        for i in range(0x0000, 0x0100, 4):
            y_pos = self.mem.read_byte(i+0xFE00)
            x_pos = self.mem.read_byte(i+0xFE00)
            pat = self.mem.read_byte(i+0xFE00)
            b3 = self.mem.read_byte(i+0xFE00)
            if y_pos==0 and x_pos ==0:
                continue
            else:
                print("sprite at {0:04X}".format(i))

    # invalidating a tile will make it be redrawn next time
    def invalidate_tile(self, tile):
        self.invalid_tiles.append(tile)

    def clear_screen(self):
        pass
#        for i in range(0, 256):
#            for j in range(0, 256):
#                self.win.plot(i,j,self.white)
#        self.win.update()
    
    def close(self):
        pass
        #self.w.close()
        #self.win.close()

    def draw_tile(self, tile, loc):
        # x
        # y
        #print("drawing tile {0:d} to {1:d}".format(tile,loc))
        offset = tile * 16
        for i in range(offset, offset+16, 2):
            line1 = self.mem.read_byte(i+0x8000) # ce00 (1011 0010 0000 0000)
            line2 = self.mem.read_byte(i+1+0x8000)
            for j in range(7, -1, -1): #draw pixels from bit 7 to 0 (LTR)
                pix1 = (line1 >> j) & (0b00000001) # 0000 0001
                pix2 = (line2 >> j) & (0b00000001) # 0000 0000
                sum = pix1 + pix2
                if sum == 0:
                    color = self.white
                elif sum == 1:
                    color = self.light_grey
                elif sum == 2:
                    color = self.dark_grey
                elif sum == 3:
                    color = self.black
                else:
                    print("j:\t{0:d}".format(j))
                    print("line1:\t{0:08b}".format(line1))
                    print("line2:\t{0:08b}".format(line2))
                    print("pix1:\t{0:08b}".format(pix1))
                    print("pix2:\t{0:08b}".format(pix2))
                    print("color:\t{0:08b}".format(sum))
                    raise Exception("color addition error")
                #print("drawing pixel on x:{0:03d} y:{1:03d} color: {2:s}".format( ((7-j) + (int(i / 16) * 8)) % 256, ((int(i / 2) % 8) + (int(i/512) * 8)), str(color) ))
                #           pixels   horizontal_tile             pixels             vertical_tile
                # location = 
                # x = (loc % 32) * 8
                # y = (loc / 32) * 8
                x = (7-j) + ((loc % 32) * 8)
                y = (int(loc / 32) * 8) + int(i / 2) 
                
                try:
                    self.bg_pix[ (7-j) + ((loc % 32) * 8), (int(loc / 32) * 8) + (int(i / 2) % 8 )] = color
                except IndexError as e:
                    print("x:{0:d} y:{1:d} color:{2:s} loc:{3:d} i:{4:d} j:{5:d}".format(x, y, str(color), loc, i, j))
                    print(e)
                    raise e

