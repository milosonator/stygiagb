import logging
from video import Video
from utils import hex2int

class Memory:

    def __init__(self):
        self.memory = bytearray(65536)
        self.vid_mem_invalid = True
        for i in range(0, len(self.memory)):
            self.memory[i] = 0

    # load a 'bytes' object into the memory
    def load(self, data, offset, start=0, end=0xFFFF):
        #logging.debug("loading data into loc %X and up." % offset)
        for i in range(max(0,start), min(len(data), end)):
            self.memory[int(i+offset)] = data[i]

    # gets an 16bit int addr and returns the 8bit int content of memory
    def read_byte(self, addr):
        if addr == 0xFF0F: # interrrupt something
            #print("byte read from interrupt flag (IF)")
            pass
        if addr == 0xFF44: # LY
            #print("byte read from LY")
            pass

        return self.memory[addr]

    # gets an 16bit int addr and returns the 16 bit int content of memory
    def read_word(self, addr):
        #self.memprint(addr, addr, 1)
        #self.memprint(addr, addr+1, 1)
        val = (self.memory[addr] | (self.memory[addr+1] << 8))
        #logging.debug("returning value: {0:0X}".format(val))
        return val
    
    def write_byte(self, addr, val):
        #if addr >= VIDEO_START <= VIDEO_END:
        #    self.video.write_byte()
        self.memory[addr] = val
        if addr == 0xFF0F: # interrrupt something
            print("byte written to interrupt flag (IF)")
        if addr >= 0x8000 or addr <= 0x8FFF:
            self.vid_mem_invalid = True
        elif addr >= 0xFE00 or addr >= 0xFE9f:
            print("byte written to OBJ memory")
        elif addr == 0xFF42:
            print("byte written to SCY: {0:02X}".format(val))
        elif addr == 0xFF43:
            print("byte written to SCX: {0:02X}".format(val))
        elif addr == 0xFF4A:
            print("byte written to WY: {0:02X}".format(val))
        elif addr == 0xFF4B:
            print("byte written to WX: {0:02X}".format(val))
        #self.memprintat(addr)

    def write_word(self, addr, val):
        self.write_byte(addr, (val & 0x00FF))
        self.write_byte(addr-1, ((val & 0xFF00) >> 8))

        #self.memprintat(addr-1)
        #self.memprintat(addr)

    def video_memory_invalidated(self):
        return self.vid_mem_invalid
    
    def validate_video_memory(self):
        self.vid_mem_invalid = False

    # print bytes bytes of the memory, starting at offset
    def memprint(self, offset, bytes, mode="hex"):
        if mode == "hex":
            for i in range(0, min(bytes,0xFFFF), 2):
                print("Mem {0:04X}:\t{1:02X}\t{2:02X}".format(i+offset, self.memory[i+offset], self.memory[i+1+offset]))
        elif mode == "bin":
            for i in range(0, bytes):
                print("Mem {0:04X}:\t{1:08b}".format(i+offset, self.memory[i+offset]))

    def memdump(self):
        with open("memdump.hex", mode='wb') as f:
           f.write(self.memory)

    def memprintat(self, offset, mode="hex"):
        if mode == "hex":
                print("Mem {0:04X}:\t{1:02X}".format(offset, self.memory[offset]))
        elif mode == "bin":
                print("Mem {0:04X}:\t{1:08b}".format(offset, self.memory[offset]))

