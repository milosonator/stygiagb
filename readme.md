#Stygia GB - A homemade python gameboy emulator.

Current status: Building CPU instructions

Watch the videos how I build this: [YouTube playlist](https://www.youtube.com/playlist?list=PLEDYD952XcG7RwWeshln0dGYdfwqt24Ps)

Docs used:

[GBCPUman](http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf)

[GBSpec](http://www.devrs.com/gb/files/gbspec.txt)

[pandocs](http://bgb.bircd.org/pandocs.htm)

[GBCPU_Instr](http://www.devrs.com/gb/files/GBCPU_Instr.html)

[About the Bootstrap](http://gbdev.gg8.se/wiki/articles/Gameboy_Bootstrap_ROM)

[More detailed about the bootstrap](https://realboyemulator.wordpress.com/2013/01/03/a-look-at-the-game-boy-bootstrap-let-the-fun-begin/)

[Opcode table](http://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html)

[gomeboycolor](https://djhworld.github.io/gomeboycolor/)