#python 34
from utils import hex2int as h2i
import logging

class Cpu:
    # commands

    # increment register
    # A: 0101 1111 ( +1 )
    # A: 0110 0000
    def _INC_n(self, args):
        val =  (self.get_register_8(args[0]) + 1) & 0x00FF
        if val == 0:
            self._set_flag('Z')
        else:
            self._set_flag('Z', False)

        if (self.get_register_8(args[0]) & 0b00001111) == 0b00001111:
            self._set_flag('H')
        else:
            self._set_flag('H', False)

        self._set_flag('N', False)
        self.set_register_8(args[0], val)

    def _INC_HL_ind(self, args):
        val = (self._mem.read_byte(self._get_HL()) + 1) & 0x00FF
        if val == 0:
            self._set_flag('Z')
        else:
            self._set_flag('Z', False)

        if (self.get_register_8(args[0]) & 0b00001111) == 0b00001111:
            self._set_flag('H')
        else:
            self._set_flag('H', False)

        self._set_flag('N', False)
        self._mem.write_byte(self._get_HL(), val)
 
    # decrement register (sets only flags)
    def _DEC_n(self, args):
        if args[0] == 1:
            self._set_flag('Z')
        else:
            self._set_flag('Z', False)
        # Half carry
        if (args[0] & 0b00001111) > 1: # set if no borrow from bit 4
            self._set_flag('H')
        else:
            self._set_flag('H', False)
        # set N flag
        self._set_flag('N')
    
    def _DEC_reg(self, args):
        self._DEC_n([self.get_register_8(args[0])])
        self.set_register_8(args[0], (self.get_register_8(args[0]) - 1) & 0x00FF)
   
    def _DEC_ind_HL(self, args):
        self._DEC_n(self._mem.read_byte(self.get_register_16('HL'))) # read indirect HL
        self._mem.write_byte(self.get_register_16('HL'), (self._mem.read_byte(self.get_register_16('HL')) -1 ) & 0x00FF) # read and dec and store

    def _INC_16_nn(self, args):
        self.set_register_16(args[0], (self.get_register_16(args[0])+1) & 0x00FFFF)

    def _SUB_A_n(self, args):
        val = (self._get_A() - args[0]) & 0x00FF
        if self._get_A() > args[0]: # no borrow: carry set
            self._set_flag('C')
        else:
            self._set_flag('C', False)
        if self._get_A() == args[0]:
            self._set_flag('Z')
        else:
            self._set_flag('Z', False)
        # Half carry
        if (self._get_A() & 0b00001111) > (args[0] & 0b00001111): # same as carry but masked
            self._set_flag('H')
        else:
            self._set_flag('H', False)
        # set N flag
        self._set_flag('N')
        self._set_A(val)

    def _SUB_A_reg(self, args):
        self._SUB_A_n([self.get_register_8(args[0])])
   
    def _SUB_A_ind_HL(self, args):
        self._SUB_A_n([self._mem.read_byte(self._get_HL())])
   
    def _ADD_A_n(self, args):
        val = self._get_A() + args[0]
        if val > 0xff: # carry set
            self._set_flag('C')
        else:
            self._set_flag('C', False)
        if val == 0:
            self._set_flag('Z')
        else:
            self._set_flag('Z', False)
        # Half carry
        if ((self._get_A() & 0b00001111) + (args[0] & 0b00001111)) > 0b00001111: # same as carry but masked
            self._set_flag('H')
        else:
            self._set_flag('H', False)
        # set N flag
        self._set_flag('N', False)
        self._set_A(val & 0x00FF)

    def _ADD_A_reg(self, args):
        self._ADD_A_n([self.get_register_8(args[0])])

    def _ADD_A_ind_HL(self, args):
        self._ADD_A_n([self._mem.read_byte(self._get_HL())])

    # jumps: arg is treated as a signed int, so it needs conversion. PC is still added after instr
    def _JR_NZ_n(self, args):
        if not (self._get_F() & 0b10000000):
            signed = (args[0] + 2**7) % 2**8 - 2**7
            jump_to = ((signed + self._get_PC()) & 0x00FF)
            self._set_PC(jump_to)

    def _JR_Z_n(self, args):
        if self._get_F() & 0b10000000:
            signed = (args[0] + 2**7) % 2**8 - 2**7
            jump_to = ((signed + self._get_PC()) & 0x00FF)
            self._set_PC(jump_to)

    def _JR_NC_n(self, args):
        if not (self._get_F() & 0b00010000):
            signed = (args[0] + 2**7) % 2**8 - 2**7
            jump_to = ((signed + self._get_PC()) & 0x00FF)
            self._set_PC(jump_to)

    def _JR_C_n(self, args):
        if (self._get_F() & 0b00010000):
            signed = (args[0] + 2**7) % 2**8 - 2**7
            jump_to = ((signed + self._get_PC()) & 0x00FF)
            self._set_PC(jump_to)

    def _JR_n(self, args):
        signed = (args[0] + 2**7) % 2**8 - 2**7
        jump_to = ((signed + self._get_PC()) & 0x00FF)
        self._set_PC(jump_to)

    def _LD_16_n_nn(self, args):
        n = args[0]
        nn = args[1]
        #logging.debug("operand: {0:0X}".format(nn))
        self.set_register_16(n, nn)

    def _LD_8_A_n(self, args):
        n = args[0]
        self._set_A(self.get_register_8(n))

    def _LD_8_A_v(self, args):
        self._set_A(args[0])
 
    def _LD_8_nn_A(self, args):
        self._mem.write_byte(self.get_register_16(args[0]), self._get_A())
    
    def _LD_8_ii_A(self, args):
        self._mem.write_byte(args[0], self._get_A())

    def _LD_I8_HL_n(self, args):
        self._mem.write_byte(self._get_HL(), self._get_register_8(args[0]))

    def _LD_I8_HL_nn(self, args):
        self._mem.write_byte(self._get_HL(), args[0])

    def _LD_I8_n_HL(self, args):
        self._set_register_8(args[0], self._mem.read_byte(self._get_HL()))

    def _LD_8_A_D(self, args):
        self._set_A(args[0])

    def _LD_8_nn_n(self, args):
        self.set_register_8(args[0], args[1])

    def _LD_8_r1_r2(self, args):
        self.set_register_8(args[0], self.get_register_8(args[1]))

    def _LDH_A_n(self, args):
        #print("Address: {0:06X}".format((0xFF00 + args[0]) & 0xFFFF))
        self._set_A(self._mem.read_byte((0xFF00 + args[0]) & 0xFFFF))

    def _XOR_A_n(self, args):
        reg=args[0]

        res = self._get_A() ^ self.get_register_8(reg)
        if res == 0:
            self._set_flag('Z')
        self._set_A(res)

    def _LDD_8_HL_A(self, args):
        self._mem.write_byte(self._get_HL(), self._get_A())
        self._set_HL(self._get_HL() - 1)

    def _LDI_8_HL_A(self, args):
        self._mem.write_byte(self._get_HL(), self._get_A())
        self._set_HL((self._get_HL() + 1) & 0x00FFFF)

    def _LD_8_IC_A(self, args):
        addr = 0xFF00 + self._get_C()
        self._mem.write_byte(addr, self._get_A())

    def _CB(self, args):
        #logging.debug("CB OPCODE: {0:02X}".format(args[0]))
        #logging.debug("CB check: {0:02X}".format(args[0] & 0b01110000))
        if (args[0] & 0b11110000) == 0b01110000:
            # BIT 7
            if (args[0] & 0b00001111) == 0b00001100:
                # H register
                #logging.debug("checking bit 7, H register")
                self._set_flag('Z', val=not (self._get_H() & 0b10000000))
            else:
                raise Exception("CB instruction not implemented yet. (inner) code: {0:02X} {0:08b}".format(args[0]))
        elif (args[0] & 0b11110000) == 0b00010000: # 0x1x RL trough carry
            if (args[0] & 0b00001111) == 0b00000000: #0x10
                self._RLC('B')
            elif (args[0] & 0b00001111) == 0b00000001: #0x11
                self._RLC('C')
            elif (args[0] & 0b00001111) == 0b00000010: #0x12
                self._RLC('D')
            elif (args[0] & 0b00001111) == 0b00000011: #0x13
                self._RLC('E')
            elif (args[0] & 0b00001111) == 0b00000100: #0x14
                self._RLC('H')
            elif (args[0] & 0b00001111) == 0b00000101: #0x15
                self._RLC('L')
            elif (args[0] & 0b00001111) == 0b00000110: #0x16
                raise Exception("CB instruction not implemented. code: {0:02X}".format(args[0]))
            elif (args[0] & 0b00001111) == 0b00000111: #0x17
                self._RLC('A')
            else:
                raise Exception("CB instruction error. code: {0:02X}".format(args[0]))

        else:
            raise Exception("CB instruction not implemented yet. (outer) code: {0:02X} {0:08b}".format(args[0]))
         
 
    def _LD_8_I_A(self, args):
        self._mem.write_byte(0xFF00 + args[0], self._get_A())
        print("writing to 0xFF{0:02X}, value: {1:02X}".format(args[0], self._get_A()))

    # push double register onto stack and decrement stack pointer twice
    # for both the push and pull it is the case that the order in which the values are pushed
    # is reversed, so if you look at the memory the pushed value 0x01234 will look like 0x3412
    # take note of the order in which the registers are stored and what registers are accessed
    def _PUSH_nn(self, args):
        self._set_SP(self._get_SP()-1)
        self._mem.write_byte(self._get_SP(), self.get_register_8(args[0][0]))
        self._set_SP(self._get_SP()-1)
        self._mem.write_byte(self._get_SP(), self.get_register_8(args[0][1]))

        #self._mem.memprint(self._get_SP(), 0xFFFF - self._get_SP())

    def _POP_nn(self, args):
        self.set_register_8(args[0][1], self._mem.read_byte(self._get_SP()))
        self._set_SP(self._get_SP()+1)
        self.set_register_8(args[0][0], self._mem.read_byte(self._get_SP()))
        self._set_SP(self._get_SP()+1)

        #self._mem.memprint(self._get_SP(), 0xFFFF - self._get_SP())

    def _RET(self, args):
        #print("returning")
        #print("SP:\t{0:04X}".format(self._get_SP()))
        C = self._mem.read_byte(self._get_SP())
        self._set_SP(self._get_SP()+1)
        P = self._mem.read_byte(self._get_SP())
        self._set_SP(self._get_SP()+1)
        self._set_PC((P << 8) | C)
        #self._mem.memprint(self._get_SP(), 0xFFFF - self._get_SP())

    # does 3 things: 1: push PC+3, 2: set PC to nn
    def _CALL_nn(self, args):
        pc = self._get_PC() + 3

        self._set_SP(self._get_SP()-1)
        self._mem.write_byte(self._get_SP(), pc >> 8) # first high part, see push
        self._set_SP(self._get_SP()-1)
        self._mem.write_byte(self._get_SP(), pc & 0x00FF) # then the low part

        self._set_PC(args[0])

    # rotate A through carry. reset N, H, set Z if zero, set C to old bit 7 of A
    def _RLC(self, args):
        if 0b00010000 & self._get_F() == 0b00010000: # if carry is set
            #first bit of new A is 1
            self.set_register_8(args[0], ((self.get_register_8(args[0]) << 1) | 0b00000001) & 0x00FF)
        else:
            #first bit of new A is 0
            self.set_register_8(args[0], ((self.get_register_8(args[0]) << 1) & 0b11111110) & 0x00FF)

        self._set_flag('C', (0b10000000 & self.get_register_8(args[0]) == 0b10000000))
        self._set_flag('Z', self._get_A() == 0)

    # compare A with n. like subtract but there is no result
    def _CP_n(self, args):
        if self._get_A() > args[0]: # no borrow: carry set
            self._set_flag('C')
        else:
            self._set_flag('C', False)
        if self._get_A() == args[0]:
            self._set_flag('Z')
        else:
            self._set_flag('Z', False)
        # Half carry
        if (self._get_A() & 0b00001111) > (args[0] & 0b00001111): # same as carry but masked
            self._set_flag('H')
        else:
            self._set_flag('H', False)
        # set N flag
        self._set_flag('N')

    def _CP_reg(self, args):
        self._CP_n([self.get_register_8(args[0])])
 
    def _DI(self, args):
        pass

    # setters
    def _set_A(self, val):
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        self._A = val
    def _set_B(self, val):
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        self._B = val
    def _set_C(self, val):
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        self._C = val
    def _set_D(self, val):
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        self._D = val
    def _set_E(self, val):
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        self._E = val
    def _set_H(self, val):
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        self._H = val
    def _set_L(self, val):
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        self._L = val
    def _set_F(self, val):
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        self._F = val

    # explanation of these operations:
        # 0x  A2        B6
        # 0b  1010 0010 1011 0110
        # H:
        # >>8 xxxx xxxx 1010 0010
        # &   0000 0000 1111 1111
        # L:
        # &   0000 0000 1011 0110
        # desired:
        # H:  1010 0010
        # L:  1011 0110  
    def _set_AF(self, val):
        self._A = (val >> 8) & 0b0000000011111111
        self._F = (val & 0b0000000011111111)
    def _set_BC(self, val):
        self._B = (val >> 8) & 0b0000000011111111
        self._C = (val & 0b0000000011111111)
    def _set_DE(self, val):
        self._D = (val >> 8) & 0b0000000011111111
        self._E = (val & 0b0000000011111111)
    def _set_HL(self, val):
        self._H = (val >> 8) & 0b0000000011111111
        self._L = (val & 0b0000000011111111)
    def _set_SP(self, val):
        self._SP = val
    def _set_PC(self, val):
        self._PC = val

    # getters
    def _get_A(self):
        return self._A
    def _get_B(self):
        return self._B
    def _get_C(self):
        return self._C
    def _get_D(self):
        return self._D
    def _get_E(self):
        return self._E
    def _get_H(self):
        return self._H
    def _get_L(self):
        return self._L
    def _get_F(self):
        return self._F

    def _get_AF(self):
        return ((self._A << 8) | self._F)
    def _get_BC(self):
        return ((self._B << 8) | self._C)
    def _get_DE(self):
        return ((self._D << 8) | self._E)
    def _get_HL(self):
        return ((self._H << 8) | self._L)
    def _get_SP(self):
        return self._SP
    def _get_PC(self):
        return self._PC

    # set register: reg: str, val: int
    def set_register_8(self, reg, val):
        #create dict with registers as key and functions as vals (sort of a jump table)
        if val > 0xFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        fns = {
            'A': self._set_A,
            'B': self._set_B,
            'C': self._set_C,
            'D': self._set_D,
            'E': self._set_E,
            'H': self._set_H,
            'L': self._set_L,
            'F': self._set_F
        }
        fn = fns[reg]
        fn(val) # exec the appropriate function 

    def set_register_16(self, reg, val):
        if val > 0xFFFF:
            raise Exception("value may not be larger than 8 bit (256)(0xFF)")
        fns = {
            'AF': self._set_AF,
            'BC': self._set_BC,
            'DE': self._set_DE,
            'HL': self._set_HL,
            'SP': self._set_SP,
            'PC': self._set_PC
        }
        fn = fns[reg]
        fn(val)

    # get value of register
    def get_register_8(self, reg):
        fns = {
            'A': self._get_A,
            'F': self._get_F,
            'B': self._get_B,
            'C': self._get_C,
            'D': self._get_D,
            'E': self._get_E,
            'H': self._get_H,
            'L': self._get_L
        }
        #self.print_registers()
        #self._mem.memprintat(self._get_DE())
        fn = fns[reg]
        return fn()

    def get_register_16(self, reg):
        fns = {
            'AF': self._get_AF,
            'BC': self._get_BC,
            'DE': self._get_DE,
            'HL': self._get_HL,
            'SP': self._get_SP,
            'PC': self._get_PC
        }
        fn = fns[reg]
        return fn()


    # set a flag ('Z','N','H','C')
    def _set_flag(self, flag, val=True):
        flgs = {
            'Z': 0b10000000,
            'N': 0b01000000,
            'H': 0b00100000,
            'C': 0b00010000
        }
        c_flgs = {
            'Z': 0b01111111,
            'N': 0b10111111,
            'H': 0b11011111,
            'C': 0b11101111
        }
        F = self._get_F() | flgs[flag]
        #logging.debug("FLAGS: 0x{0:0X}".format(F))
        #logging.debug("Val: 0x{0:02X}".format(val))
        #logging.debug("type: " + str(type(val)))
        if val: # set flag
            #logging.debug("setting flag")
            self._set_F(self._get_F() | flgs[flag])
        else:   # clear/reset flag
            #logging.debug("resetting flag")
            self._set_F(self._get_F() & c_flgs[flag])
        #logging.debug("FLAGS: {0:0X}".format(F))
        

    def __init__(self, mem):
        self._mem = mem
        self._A=0
        self._F=0
        self._B=0
        self._C=0
        self._D=0
        self._E=0
        self._H=0
        self._L=0
        self._SP=0
        self._PC=0

        self.instructions = self.get_instructions()
        
        #self.set_register_8('F', h2i("B0"))
        #self.set_register_16('AF', h2i("0001"))
        #self.set_register_16('BC', h2i("0013"))
        #self.set_register_16('DE', h2i("00D8"))
        #self.set_register_16('HL', h2i("014D"))
        #self.set_register_16('SP', h2i("FFFE"))
        self.set_register_16('PC', 0)
        self.print_registers()

    def print_registers(self):
        print("AF: \t{0:04X}\t{0:016b}".format(self._get_AF()))
        print("A: \t{0:02X}\t{0:08b}".format(self._get_A()))
        print("F: \t{0:02X}\t{0:08b}".format(self._get_F()))
        print("BC: \t{0:04X}\t{0:016b}".format(self._get_BC()))
        print("DE: \t{0:04X}\t{0:016b}".format(self._get_DE()))
        print("HL: \t{0:04X}\t{0:016b}".format(self._get_HL()))
        print("SP: \t{0:04X}\t{0:016b}".format(self._get_SP()))
        print("PC: \t{0:04X}\t{0:016b}".format(self._get_PC()))


    def execute(self):
        #get the instruction opcode from the PC
        instr = self._mem.read_byte(self._get_PC())
        args = []

        #decoding
        try: 
            command = self.instructions[instr]
        except KeyError as e:
            print ("Instruction not found")
            print ("instr: {0:02X}".format(instr))
            self.print_registers()
            self._mem.memdump()
            raise e
        #logging.debug("Current instruction: {0:04X}".format(instr))
        #fetch additional
        
        # get operand from register
        if command.get('register', False):
            args.append(command['register'])

        # immediate: get operand(s) from program memory
        if command.get('immediate_16', False):
            args.append(self._mem.read_word(self._get_PC()+1))
        elif command.get('immediate_8', False):
            args.append(self._mem.read_byte(self._get_PC()+1))

        if command.get('registers', False):
            args.extend(command['registers'])

        # get the operand from the address specified in command[indirect]
        if command.get('indirect'):
            args.append(self._mem.read_byte(self.get_register_16(command['indirect'])))
        
        #execute the command
        fn = command['fn']
        fn(args)
        #increment PC 
        self._set_PC(self._get_PC()+command['PC'])
        return command['cycles']

    def get_instructions(self):
        return {
            0x01: {
                'fn': self._LD_16_n_nn,
                'register': 'BC',
                'immediate_16': True,
                'cycles': 12,
                'PC': 3
            },
            0x02: {
                'fn': self._LD_8_nn_A,
                'register': 'BC',
                'cycles': 8,
                'PC': 1
            },
            0x03: {
                'fn': self._INC_16_nn,
                'register': 'BC',
                'cycles': 8,
                'PC': 1
            },
            0x04: {
                'fn': self._INC_n,
                'register': 'B',
                'cycles': 4,
                'PC': 1
            },
            0x05: {
                'fn': self._DEC_reg,
                'register': 'B',
                'cycles': 4,
                'PC': 1
            },
            0x06: {
                'fn': self._LD_8_nn_n,
                'register': 'B',
                'immediate_8': True,
                'PC': 2,
                'cycles': 8
            },
            0x0A: {
                'fn': self._LD_8_A_v,
                'cycles': 8,
                'indirect': 'BC',
                'PC': 1
            },
            0x0C: {
                'fn': self._INC_n,
                'register': 'C',
                'cycles': 4,
                'PC': 1
            },
            0x0D: {
                'fn': self._DEC_reg,
                'register': 'C',
                'cycles': 4,
                'PC': 1
            },
            0x0E: {
                'fn': self._LD_8_nn_n,
                'register': 'C',
                'immediate_8': True,
                'PC': 2,
                'cycles': 8
            },
            0x11: {
                'fn': self._LD_16_n_nn,
                'register': 'DE',
                'immediate_16': True,
                'cycles': 12,
                'PC': 3
            },
            0x12: {
                'fn': self._LD_8_nn_A,
                'register': 'DE',
                'cycles': 8,
                'PC': 1
            },
            0x13: {
                'fn': self._INC_16_nn,
                'register': 'DE',
                'cycles': 8,
                'PC': 1
            },
            0x14: {
                'fn': self._INC_n,
                'register': 'D',
                'cycles': 4,
                'PC': 1
            },
            0x15: {
                'fn': self._DEC_reg,
                'register': 'D',
                'cycles': 4,
                'PC': 1
            },
            0x16: {
                'fn': self._LD_8_nn_n,
                'register': 'D',
                'immediate_8': True,
                'PC': 2,
                'cycles': 8
            },
            0x17: {
                'fn': self._RLC,
                'register': 'A',
                'PC': 1,
                'cycles': 4
            },
            0x18: {
                'fn': self._JR_n,
                'immediate_8': True,
                'cycles': 8,
                'PC': 2
            },
            0x1A: {
                'fn': self._LD_8_A_v,
                'cycles': 8,
                'indirect': 'DE',
                'PC': 1
            },
            0x1C: {
                'fn': self._INC_n,
                'register': 'E',
                'cycles': 4,
                'PC': 1
            },
            0x1D: {
                'fn': self._DEC_reg,
                'register': 'E',
                'cycles': 4,
                'PC': 1
            },
            0x1E: {
                'fn': self._LD_8_nn_n,
                'register': 'E',
                'immediate_8': True,
                'PC': 2,
                'cycles': 8
            },
            0x20: {
                'fn': self._JR_NZ_n,
                'immediate_8': True,
                'cycles': 8,
                'PC': 2
            },
            0x21: {
                'fn': self._LD_16_n_nn,
                'register': 'HL',
                'immediate_16': True,
                'cycles': 12,
                'PC': 3
            },
            0x22: {
                'fn': self._LDI_8_HL_A,
                'cycles': 8,
                'PC': 1
            },
            0x23: {
                'fn': self._INC_16_nn,
                'register': 'HL',
                'cycles': 8,
                'PC': 1
            },
            0x24: {
                'fn': self._INC_n,
                'register': 'H',
                'cycles': 4,
                'PC': 1
            },
            0x25: {
                'fn': self._DEC_reg,
                'register': 'H',
                'cycles': 4,
                'PC': 1
            },
            0x26: {
                'fn': self._LD_8_nn_n,
                'register': 'H',
                'immediate_8': True,
                'PC': 2,
                'cycles': 8
            },
            0x28: {
                'fn': self._JR_Z_n,
                'immediate_8': True,
                'cycles': 8,
                'PC': 2
            },
            0x2C: {
                'fn': self._INC_n,
                'register': 'L',
                'cycles': 4,
                'PC': 1
            },
            0x2D: {
                'fn': self._DEC_reg,
                'register': 'L',
                'cycles': 4,
                'PC': 1
            },
            0x2E: {
                'fn': self._LD_8_nn_n,
                'register': 'L',
                'immediate_8': True,
                'PC': 2,
                'cycles': 8
            },
            0x30: {
                'fn': self._JR_NC_n,
                'immediate_8': True,
                'cycles': 8,
                'PC': 2
            },
            0x31: {
                'fn': self._LD_16_n_nn,
                'register': 'SP',
                'immediate_16': True,
                'cycles': 12,
                'PC': 3
            },
            0x32: {
                'fn': self._LDD_8_HL_A,
                'cycles': 8,
                'PC': 1
            },
            0x33: {
                'fn': self._INC_16_nn,
                'register': 'SP',
                'cycles': 8,
                'PC': 1
            },
            0x34: {
                'fn': self._INC_HL_ind,
                'cycles': 12,
                'PC': 1
            },
            0x35: {
                'fn': self._DEC_ind_HL,
                'cycles': 12,
                'PC': 1
            },
            0x36: {
                'fn': self._LD_I8_HL_nn,
                'immediate_8': True,
                'cycles': 12,
                'PC': 2
            },
            0x38: {
                'fn': self._JR_C_n,
                'immediate_8': True,
                'cycles': 8,
                'PC': 2
            },
            0x3D: {
                'fn': self._DEC_reg,
                'register': 'A',
                'cycles': 4,
                'PC': 1
            },
            0x3E: {
                'fn': self._LD_8_A_v,
                'immediate_8': True,
                'cycles': 8,
                'PC': 2
            },
            0x40: {
                'fn': self._LD_8_r1_r2,
                'registers': ['B','B'],
                'cycles': 4,
                'PC': 1
            },
            0x41: {
                'fn': self._LD_8_r1_r2,
                'registers': ['B','C'],
                'cycles': 4,
                'PC': 1
            },
            0x42: {
                'fn': self._LD_8_r1_r2,
                'registers': ['B','D'],
                'cycles': 4,
                'PC': 1
            },
            0x43: {
                'fn': self._LD_8_r1_r2,
                'registers': ['B','E'],
                'cycles': 4,
                'PC': 1
            },
            0x44: {
                'fn': self._LD_8_r1_r2,
                'registers': ['B','H'],
                'cycles': 4,
                'PC': 1
            },
            0x45: {
                'fn': self._LD_8_r1_r2,
                'registers': ['B','L'],
                'cycles': 4,
                'PC': 1
            },
            0x46: {
                'fn': self._LD_I8_n_HL,
                'register': ['B'],
                'cycles': 8,
                'PC': 1
            },
            0x47: {
                'fn': self._LD_8_r1_r2,
                'registers': ['B','A'],
                'cycles': 4,
                'PC': 1
            },
            0x48: {
                'fn': self._LD_8_r1_r2,
                'registers': ['C','B'],
                'cycles': 4,
                'PC': 1
            },
            0x49: {
                'fn': self._LD_8_r1_r2,
                'registers': ['C','C'],
                'cycles': 4,
                'PC': 1
            },
            0x4A: {
                'fn': self._LD_8_r1_r2,
                'registers': ['C','D'],
                'cycles': 4,
                'PC': 1
            },
            0x4B: {
                'fn': self._LD_8_r1_r2,
                'registers': ['C','E'],
                'cycles': 4,
                'PC': 1
            },
            0x4C: {
                'fn': self._LD_8_r1_r2,
                'registers': ['C','H'],
                'cycles': 4,
                'PC': 1
            },
            0x4D: {
                'fn': self._LD_8_r1_r2,
                'registers': ['C','L'],
                'cycles': 4,
                'PC': 1
            },
            0x4E: {
                'fn': self._LD_I8_n_HL,
                'register': ['C'],
                'cycles': 8,
                'PC': 1
            },
            0x4F: {
                'fn': self._LD_8_r1_r2,
                'registers': ['C','A'],
                'cycles': 4,
                'PC': 1
            },
            0x50: {
                'fn': self._LD_8_r1_r2,
                'registers': ['D','B'],
                'cycles': 4,
                'PC': 1
            },
            0x51: {
                'fn': self._LD_8_r1_r2,
                'registers': ['D','C'],
                'cycles': 4,
                'PC': 1
            },
            0x52: {
                'fn': self._LD_8_r1_r2,
                'registers': ['D','D'],
                'cycles': 4,
                'PC': 1
            },
            0x53: {
                'fn': self._LD_8_r1_r2,
                'registers': ['D','E'],
                'cycles': 4,
                'PC': 1
            },
            0x54: {
                'fn': self._LD_8_r1_r2,
                'registers': ['D','H'],
                'cycles': 4,
                'PC': 1
            },
            0x55: {
                'fn': self._LD_8_r1_r2,
                'registers': ['D','L'],
                'cycles': 4,
                'PC': 1
            },
            0x56: {
                'fn': self._LD_I8_n_HL,
                'register': ['D'],
                'cycles': 8,
                'PC': 1
            },
            0x57: {
                'fn': self._LD_8_r1_r2,
                'registers': ['D','A'],
                'cycles': 4,
                'PC': 1
            },
            0x58: {
                'fn': self._LD_8_r1_r2,
                'registers': ['E','B'],
                'cycles': 4,
                'PC': 1
            },
            0x59: {
                'fn': self._LD_8_r1_r2,
                'registers': ['E','C'],
                'cycles': 4,
                'PC': 1
            },
            0x5A: {
                'fn': self._LD_8_r1_r2,
                'registers': ['E','D'],
                'cycles': 4,
                'PC': 1
            },
            0x5B: {
                'fn': self._LD_8_r1_r2,
                'registers': ['E','E'],
                'cycles': 4,
                'PC': 1
            },
            0x5C: {
                'fn': self._LD_8_r1_r2,
                'registers': ['E','H'],
                'cycles': 4,
                'PC': 1
            },
            0x5D: {
                'fn': self._LD_8_r1_r2,
                'registers': ['E','L'],
                'cycles': 4,
                'PC': 1
            },
            0x5E: {
                'fn': self._LD_I8_n_HL,
                'register': ['E'],
                'cycles': 8,
                'PC': 1
            },
            0x5F: {
                'fn': self._LD_8_r1_r2,
                'registers': ['E','A'],
                'cycles': 4,
                'PC': 1
            },
            0x60: {
                'fn': self._LD_8_r1_r2,
                'registers': ['H','B'],
                'cycles': 4,
                'PC': 1
            },
            0x61: {
                'fn': self._LD_8_r1_r2,
                'registers': ['H','C'],
                'cycles': 4,
                'PC': 1
            },
            0x62: {
                'fn': self._LD_8_r1_r2,
                'registers': ['H','D'],
                'cycles': 4,
                'PC': 1
            },
            0x63: {
                'fn': self._LD_8_r1_r2,
                'registers': ['H','E'],
                'cycles': 4,
                'PC': 1
            },
            0x64: {
                'fn': self._LD_8_r1_r2,
                'registers': ['H','H'],
                'cycles': 4,
                'PC': 1
            },
            0x65: {
                'fn': self._LD_8_r1_r2,
                'registers': ['H','L'],
                'cycles': 4,
                'PC': 1
            },
            0x66: {
                'fn': self._LD_I8_n_HL,
                'register': ['H'],
                'cycles': 8,
                'PC': 1
            },
            0x67: {
                'fn': self._LD_8_r1_r2,
                'registers': ['H','A'],
                'cycles': 4,
                'PC': 1
            },
            0x68: {
                'fn': self._LD_8_r1_r2,
                'registers': ['L','B'],
                'cycles': 4,
                'PC': 1
            },
            0x69: {
                'fn': self._LD_8_r1_r2,
                'registers': ['L','C'],
                'cycles': 4,
                'PC': 1
            },
            0x6A: {
                'fn': self._LD_8_r1_r2,
                'registers': ['L','D'],
                'cycles': 4,
                'PC': 1
            },
            0x6B: {
                'fn': self._LD_8_r1_r2,
                'registers': ['L','E'],
                'cycles': 4,
                'PC': 1
            },
            0x6C: {
                'fn': self._LD_8_r1_r2,
                'registers': ['L','H'],
                'cycles': 4,
                'PC': 1
            },
            0x6D: {
                'fn': self._LD_8_r1_r2,
                'registers': ['L','L'],
                'cycles': 4,
                'PC': 1
            },
            0x6E: {
                'fn': self._LD_I8_n_HL,
                'register': ['L'],
                'cycles': 8,
                'PC': 1
            },
            0x6F: {
                'fn': self._LD_8_r1_r2,
                'registers': ['L','A'],
                'cycles': 4,
                'PC': 1
            },
            0x70: {
                'fn': self._LD_I8_HL_n,
                'register': 'B',
                'cycles': 8,
                'PC': 1
            },
            0x71: {
                'fn': self._LD_I8_HL_n,
                'register': 'C',
                'cycles': 8,
                'PC': 1
            },
            0x72: {
                'fn': self._LD_I8_HL_n,
                'register': 'D',
                'cycles': 8,
                'PC': 1
            },
            0x73: {
                'fn': self._LD_I8_HL_n,
                'register': 'E',
                'cycles': 8,
                'PC': 1
            },
            0x74: {
                'fn': self._LD_I8_HL_n,
                'register': 'H',
                'cycles': 8,
                'PC': 1
            },
            0x75: {
                'fn': self._LD_I8_HL_n,
                'register': 'L',
                'cycles': 8,
                'PC': 1
            },
            0x77: {
                'fn': self._LD_8_nn_A,
                'register': 'HL',
                'cycles': 8,
                'PC': 1
            },
            0x78: {
                'fn': self._LD_8_A_n,
                'cycles': 4,
                'register': 'B',
                'PC': 1
            },
            0x79: {
                'fn': self._LD_8_A_n,
                'cycles': 4,
                'register': 'C',
                'PC': 1
            },
            0x7A: {
                'fn': self._LD_8_A_n,
                'cycles': 4,
                'register': 'D',
                'PC': 1
            },
            0x7B: {
                'fn': self._LD_8_A_n,
                'cycles': 4,
                'register': 'E',
                'PC': 1
            },
            0x7C: {
                'fn': self._LD_8_A_n,
                'cycles': 4,
                'register': 'H',
                'PC': 1
            },
            0x7D: {
                'fn': self._LD_8_A_n,
                'cycles': 4,
                'register': 'L',
                'PC': 1
            },
            0x7E: {
                'fn': self._LD_8_A_v,
                'cycles': 8,
                'indirect': 'HL',
                'PC': 1
            },
            0x7F: {
                'fn': self._LD_8_A_n,
                'cycles': 4,
                'register': 'A',
                'PC': 1
            },
            0x80: {
                'fn': self._ADD_A_reg,
                'register': 'B',
                'cycles': 4,
                'PC': 1
            },
            0x81: {
                'fn': self._ADD_A_reg,
                'register': 'C',
                'cycles': 4,
                'PC': 1
            },
            0x82: {
                'fn': self._ADD_A_reg,
                'register': 'D',
                'cycles': 4,
                'PC': 1
            },
            0x83: {
                'fn': self._ADD_A_reg,
                'register': 'E',
                'cycles': 4,
                'PC': 1
            },
            0x84: {
                'fn': self._ADD_A_reg,
                'register': 'H',
                'cycles': 4,
                'PC': 1
            },
            0x85: {
                'fn': self._ADD_A_reg,
                'register': 'L',
                'cycles': 4,
                'PC': 1
            },
            0x86: {
                'fn': self._ADD_A_ind_HL,
                'cycles': 8,
                'PC': 1
            },
            0x87: {
                'fn': self._ADD_A_reg,
                'register': 'A',
                'cycles': 4,
                'PC': 1
            },
            0x90: {
                'fn': self._SUB_A_reg,
                'register': 'B',
                'cycles': 4,
                'PC': 1
            },
            0x91: {
                'fn': self._SUB_A_reg,
                'register': 'C',
                'cycles': 4,
                'PC': 1
            },
            0x92: {
                'fn': self._SUB_A_reg,
                'register': 'D',
                'cycles': 4,
                'PC': 1
            },
            0x93: {
                'fn': self._SUB_A_reg,
                'register': 'E',
                'cycles': 4,
                'PC': 1
            },
            0x94: {
                'fn': self._SUB_A_reg,
                'register': 'H',
                'cycles': 4,
                'PC': 1
            },
            0x95: {
                'fn': self._SUB_A_reg,
                'register': 'L',
                'cycles': 4,
                'PC': 1
            },
            0x96: {
                'fn': self._SUB_A_ind_HL,
                'cycles': 8,
                'PC': 1
            },
            0x97: {
                'fn': self._SUB_A_reg,
                'register': 'A',
                'cycles': 4,
                'PC': 1
            },
            0xA8: {
                'fn': self._XOR_A_n,
                'cycles': 4,
                'register': 'B',
                'flags': ['Z'],
                'PC': 1
            },
            0xA9: {
                'fn': self._XOR_A_n,
                'cycles': 4,
                'register': 'C',
                'flags': ['Z'],
                'PC': 1
            },
            0xAA: {
                'fn': self._XOR_A_n,
                'cycles': 4,
                'register': 'D',
                'flags': ['Z'],
                'PC': 1
            },
            0xAB: {
                'fn': self._XOR_A_n,
                'cycles': 4,
                'register': 'E',
                'flags': ['Z'],
                'PC': 1
            },
            0xAC: {
                'fn': self._XOR_A_n,
                'cycles': 4,
                'register': 'H',
                'flags': ['Z'],
                'PC': 1
            },
            0xAD: {
                'fn': self._XOR_A_n,
                'cycles': 4,
                'register': 'L',
                'flags': ['Z'],
                'PC': 1
            },
            0xAE: {
                'fn': self._XOR_A_n,
                'cycles': 8,
                'indirect': 'HL',
                'flags': ['Z'],
                'PC': 1
            },
            0xAF: {
                'fn': self._XOR_A_n,
                'cycles': 4,
                'register': 'A',
                'flags': ['Z'],
                'PC': 1
            },
            0xB8: {
                'fn': self._CP_reg,
                'cycles': 4,
                'register': 'B',
                'PC': 1
            },
            0xB9: {
                'fn': self._CP_reg,
                'cycles': 4,
                'register': 'C',
                'PC': 1
            },
            0xBA: {
                'fn': self._CP_reg,
                'cycles': 4,
                'register': 'D',
                'PC': 1
            },
            0xBB: {
                'fn': self._CP_reg,
                'cycles': 4,
                'register': 'E',
                'PC': 1
            },
            0xBC: {
                'fn': self._CP_reg,
                'cycles': 4,
                'register': 'H',
                'PC': 1
            },
            0xBD: {
                'fn': self._CP_reg,
                'cycles': 4,
                'register': 'L',
                'PC': 1
            },
            0xBE: {
                'fn': self._CP_n,
                'cycles': 8,
                'indirect': 'HL',
                'PC': 1
            },
            0xBF: {
                'fn': self._CP_reg,
                'cycles': 4,
                'register': 'A',
                'PC': 1
            },
            0xC1: {
                'fn': self._POP_nn,
                'register': 'BC',
                'cycles': 12,
                'PC': 1
            },
            0xC5: {
                'fn': self._PUSH_nn,
                'cycles': 16,
                'register': 'BC',
                'PC': 1
            },
            0xC6: {
                'fn': self._SUB_A_n,
                'cycles': 8,
                'immediate_8': True,
                'PC': 2
            },
            0xC9: {
                'fn': self._RET,
                'cycles': 8,
                'PC': 0
            },
            0xCB: {
                'fn': self._CB,
                'immediate_8': True,
                'cycles': 8,
                'PC': 2
            },
            0xCD: {
                'fn': self._CALL_nn,
                'immediate_16': True,
                'cycles': 12,
                'PC': 0
            },
            0xD1: {
                'fn': self._POP_nn,
                'register': 'DE',
                'cycles': 12,
                'PC': 1
            },
            0xD5: {
                'fn': self._PUSH_nn,
                'cycles': 16,
                'register': 'DE',
                'PC': 1
            },
            0xD6: {
                'fn': self._SUB_A_n,
                'immediate_8': True,
                'cycles': 8,
                'PC': 2
            },
            0xE0: {
                'fn': self._LD_8_I_A,
                'immediate_8': True,
                'cycles': 12,
                'PC': 2
            },
            0xE1: {
                'fn': self._POP_nn,
                'register': 'HL',
                'cycles': 12,
                'PC': 1
            },
            0xE2: {
                'fn': self._LD_8_IC_A,
                'cycles': 8,
                'PC': 1
            },
            0xE5: {
                'fn': self._PUSH_nn,
                'cycles': 16,
                'register': 'HL',
                'PC': 1
            },
            0xEA: {
                'fn': self._LD_8_ii_A,
                'immediate_16': True,
                'cycles': 16,
                'PC': 3
            },
            0xEE: {
                'fn': self._XOR_A_n,
                'cycles': 8,
                'immediate_8': True,
                'flags': ['Z'],
                'PC': 1
            },
            0xF0: {
                'fn': self._LDH_A_n,
                'immediate_8': True,
                'cycles': 12,
                'PC': 2
            },
            0xF1: {
                'fn': self._POP_nn,
                'register': 'AF',
                'cycles': 12,
                'PC': 1
            },
            0xF3: {
                'fn': self._DI,
                'cycles': 4,
                'PC': 1
            },
            0xF5: {
                'fn': self._PUSH_nn,
                'cycles': 16,
                'register': 'AF',
                'PC': 1
            },
            0xFA: {
                'fn': self._LD_8_A_v,
                'cycles': 16,
                'immediate_16': True,
                'PC': 3
            },
            0xFE: {
                'fn': self._CP_n,
                'cycles': 8,
                'immediate_8': True,
                'PC': 2
            },
        }
