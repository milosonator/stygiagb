20:54] == AntonioND [~Antonio@168.Red-79-148-12.dynamicIP.rima-tde.net] has quit [Quit: Leaving]
[21:05] <matja> michielha: sp points to the item last pushed. for example, for "push af", it effectively does : dec sp ; ld (sp), a ; dec sp ; ld (sp), f
[21:05] <matja> those instructions don't exist, they're just to break it down
[21:07] <matja> so if you did a 16-bit load from sp, the least significant byte is at sp
[21:07] <matja> as such, it is a little-endian cpu
[21:13] == npt [~npt@67-130-15-94.dia.static.qwest.net] has quit [Remote host closed the connection]
[21:22] <michielha> matja, thank you. so the memory will hold the registers 'in reverse'? For example:
[21:23] <michielha> if SP=0xFFFE and the command is: PUSH AF , A=0xAB, F=0xCD
[21:23] <michielha> then SP=0xFFFC and contents are: FFFC: 0xCD, FFFD: 0xAB
[21:24] <matja> yes, exactly
[21:24] <michielha> mmh i think ive been doing that wrong. Maybe also memory reads/writes even :O
[21:24] <matja> this is the same as the zilog z80 and intel 8080
[21:25] <michielha> matja: yes, ofcourse, but its not like I build an emulator for any of those either :)
