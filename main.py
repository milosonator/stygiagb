#/usr/bin/python3.4

import logging
import sys

from gpu import Gpu
from memory import Memory
from rom import Rom
from cpu import Cpu
from utils import hex2int as h2i

def init():
    #logging.basicConfig(level=logging.DEBUG)
    #initialize memory (sounds fancy, but it is just python)
    mem = Memory()
    gpu = Gpu(mem)

    cpu = Cpu(mem)
    with open("bios.rom", mode="rb") as f:
        boot = f.read() #b for binary

    mem.load(boot, h2i("0000"))
    if sys.argv[1]:
        logging.debug("argument supplied: %s" % sys.argv[1])
        with open(sys.argv[1], mode="rb") as f:
            rom = f.read()
            logging.debug(rom)
            mem.load(rom, 0x0000, start=0x0100, end=0x4000)
    else:
        logging.error("no argument supplied, supply a file")

    return cpu, mem, gpu

# arg[1] should hold the rom filename
debug = False
breakpoint = 20000000000
cpu, mem, gpu = init()

# check for debug
try:
    if sys.argv[2] == '--debug':
        print("Debug mode activated")
        debug = True
except Exception:
    pass

try:
    if sys.argv[3]:
        print("'breakpoint will be set")
        debug = False
        breakpoint = int(sys.argv[3])
    else:
        breakpoint = 20000000000
except Exception:
    pass

def loop(debug, breakpoint):
    cycles = 0
    mem.memdump()
    while(1):
        #logging.debug("starting execution")
        #logging.debug("INSTRUCTION: %s" % str(cpu.get_register_16('PC')))
        #logging.debug(cpu.print_registers())
        if cycles >= breakpoint:
            breakpoint = 2000000000
            debug = True
            mem.memdump()
        if debug:
            print("PC: 0x{0:04X} SP: 0x{1:04X} A: 0x{2:02X} B: 0x{3:02X} C: 0x{4:02X} D: 0x{5:02X} E: 0x{6:02X} H: 0x{7:02X} L: 0x{8:02X} F: 0x{9:08b} INSTR: 0x{10:02X} {11:d}".format(
            cpu.get_register_16("PC"),
            cpu.get_register_16("SP"),
            cpu.get_register_8("A"),
            cpu.get_register_8("B"),
            cpu.get_register_8("C"),
            cpu.get_register_8("D"),
            cpu.get_register_8("E"),
            cpu.get_register_8("H"),
            cpu.get_register_8("L"),
            cpu.get_register_8("F"),
            cpu._mem.read_byte(cpu.get_register_16("PC")),
            cycles
            ))
        c = cpu.execute()
        cycles += c
        if cycles % 4194304 == 0:
            print("a second has passed")
        gpu.tick(c)
        #logging.debug("end execution")
        #logging.debug(cpu.print_registers())
        #measure time.
        #wait certain amount of time

try:
    print(debug)
    print(breakpoint)
    loop(debug, breakpoint)
except Exception as e:
    mem.memdump()
    gpu.close()
    print(e)
    raise e
