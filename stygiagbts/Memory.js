/**
 * Created by michiel on 11-7-15.
 */
var Memory = (function () {
    function Memory() {
        this.mem = Uint8Array[65536];
        this.mem.forEach(function (el, i) {
            this.mem[i] = ~~(Math.random() * 255);
        });
    }
    return Memory;
})();
//# sourceMappingURL=Memory.js.map