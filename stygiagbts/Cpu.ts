///<reference path="Memory.ts"/>
///<reference path="Instructions.ts"/>
/**
 * Created by  michiel on 11-7-15.
 */
class Cpu {
    public A = 0;
    public F = 0;
    public B = 0;
    public C = 0;
    public D = 0;
    public E = 0;
    public H = 0;
    public L = 0;
    public SP = 0;
    public PC = 0;
    public mem: Memory;

    private setFlagMasks = {
        'Z': 0x80, // 0b10000000
        'N': 0x40, // 0b01000000
        'H': 0x20, // 0b00100000
        'C': 0x10  // 0b00010000
    };

    private clearFlagMasks = {
        'Z': 0x7F, // 0b01111111
        'N': 0xBF, // 0b10111111
        'H': 0xDF, // 0b11011111
        'C': 0xEF  // 0b11101111
    };

    setFlag(flag: string, value: boolean) {
        if (value) {
            this.F = (this.F | this.setFlagMasks[flag])
        } else {
            this.F = (this.F & this.clearFlagMasks[flag])
        }
    }

    getFlag(flag: string) {
        return (this.F & this.setFlagMasks[flag]) != 0
    }

    getRegister(reg: string) {
        switch (reg){
            case 'A':
                return this.A;
            case 'F':
                return this.F;
            case 'B':
                return this.B;
            case 'C':
                return this.C;
            case 'D':
                return this.D;
            case 'E':
                return this.E;
            case 'H':
                return this.H;
            case 'L':
                return this.L;
            case 'AF':
                return (this.A << 8) | this.F;
            case 'BC':
                return (this.B << 8) | this.C;
            case 'DE':
                return (this.D << 8) | this.E;
            case 'HL':
                return (this.H << 8) | this.L;
            default:
                console.log("error, wrong register supplied: ", reg);
                return 0;
        }
    }

    setRegister(reg: string, data: number) {
        switch (reg){
            case 'A':
                this.A = data;
                break;
            case 'F':
                this.F = data;
                break;
            case 'B':
                this.B = data;
                break;
            case 'C':
                this.C = data;
                break;
            case 'D':
                this.D = data;
                break;
            case 'E':
                this.E = data;
                break;
            case 'H':
                this.H = data;
                break;
            case 'L':
                this.L = data;
                break;
            case 'AF':
                this.A = (data & 0xff00) >> 8;
                this.F = (data & 0x00ff);
                break;
            case 'BC':
                this.B = (data & 0xff00) >> 8;
                this.C = (data & 0x00ff);
                break;
            case 'DE':
                this.D = (data & 0xff00) >> 8;
                this.E = (data & 0x00ff);
                break;
            case 'HL':
                this.H = (data & 0xff00) >> 8;
                this.L = (data & 0x00ff);
                break;
            default:
                console.log("error, wrong register supplied: ", reg);
                return 0;
        }
    }

    constructor(mem: Memory) {
        //nom
        this.mem = mem;
    }

    execute(): number {
        //read instruction from memory
        var instr = this.mem.readByte(this.PC);
        //console.log("instruction: 0x" + instr.toString(16));

        var commandSet = this.instructions[instr];
        if (!commandSet) this.throwError("Unsupported instruction: 0x" + instr.toString(16));
        //console.log("command decoded: ", commandSet);

        // execute command with arguments supplied by ld function
        // pass cpu ref into those functions

        //exception for CB instructions, todo: prolly also for halt and similar
        if (instr == 0xCB) {
            var cbInstr = commandSet.ld(this)[0];
            var cbCommandSet = this.cbInstructions[cbInstr]; // decode cb
            if (!cbCommandSet) this.throwError("Unsupported CB instruction: 0x" + cbInstr.toString(16));
            cbCommandSet.fn(this, cbCommandSet.bit, cbCommandSet.reg); // exec cb instruction
        } else {
            commandSet.fn(this, commandSet.ld(this));
        }

        // increase program counter
        this.PC += commandSet.pc;

        // return number of cycles spent
        return commandSet.cs || cbCommandSet.cs;
    }

    private throwError(msg: string) {
        throw new Error(msg + "\n" + this.stateString());
    }

    public stateString() {
        return "A: 0x" + this.A.toString(16) +
            "\n" + "F: 0x" + this.F.toString(16) +
            "\n" + "B: 0x" + this.B.toString(16) +
            "\n" + "C: 0x" + this.C.toString(16) +
            "\n" + "D: 0x" + this.D.toString(16) +
            "\n" + "E: 0x" + this.E.toString(16) +
            "\n" + "H: 0x" + this.H.toString(16) +
            "\n" + "L: 0x" + this.L.toString(16) +
            "\n" + "SP: 0x" + this.SP.toString(16) +
            "\n" + "PC: 0x" + this.PC.toString(16);
    }

    //loaders always return an array of args
    private register_A(cpu: Cpu) {
        return [cpu.A];
    }
    private register_B(cpu: Cpu) {
        return [cpu.B];
    }
    private register_C(cpu: Cpu) {
        return [cpu.C];
    }
    private register_D(cpu: Cpu) {
        return [cpu.D];
    }
    private register_E(cpu: Cpu) {
        return [cpu.E];
    }
    private register_H(cpu: Cpu) {
        return [cpu.H];
    }
    private register_L(cpu: Cpu) {
        return [cpu.L];
    }
    private register_BC(cpu: Cpu) {
        return [(cpu.B << 8) | cpu.C]
    }
    private register_DE(cpu: Cpu) {
        return [(cpu.D << 8) | cpu.E]
    }
    private register_HL(cpu: Cpu) {
        return [(cpu.H << 8) | cpu.L]
    }
    private register_AF(cpu: Cpu) {
        return [(cpu.A << 8) | cpu.F]
    }

    private indirect_BC(cpu: Cpu) {
        return [cpu.mem.readByte( (cpu.B << 8) | cpu.C )];
    }
    private indirect_DE(cpu: Cpu) {
        return [cpu.mem.readByte( (cpu.D << 8) | cpu.E )];
    }
    private indirect_HL(cpu: Cpu) {
        return [cpu.mem.readByte( cpu.getRegister('HL') )];
    }

    private immediate8(cpu: Cpu) {
        return [cpu.mem.readByte( cpu.PC+1 )];
    }
    private immediate16(cpu: Cpu) {
        return [cpu.mem.readWord( cpu.PC+1 )];
    }
    private indirect16(cpu: Cpu) {
        return [cpu.mem.readWord(cpu.mem.readWord( cpu.PC+1 ))];
    }

    private none(cpu: Cpu) {
        return [];
    }

    private instructions = {
        0x01: {
            fn: Instructions.LD_i16_BC,
            ld: this.immediate16,
            cs: 12,
            pc: 3
        },
        0x02: {
            fn: Instructions.LD_n_A,
            ld: this.indirect_BC,
            cs: 8,
            pc: 1
        },
        0x03: {
            fn: Instructions.INC_BC,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x04: {
            fn: Instructions.INC_B,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x05: {
            fn: Instructions.DEC_B,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x06: {
            fn: Instructions.LD_n_B,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x0A: {
            fn: Instructions.LD_n_A,
            ld: this.indirect_BC,
            cs: 8,
            pc: 1
        },
        0x0B: {
            fn: Instructions.DEC_BC,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x0C: {
            fn: Instructions.INC_C,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x0D: {
            fn: Instructions.DEC_C,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x0E: {
            fn: Instructions.LD_n_C,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x11: {
            fn: Instructions.LD_i16_DE,
            ld: this.immediate16,
            cs: 12,
            pc: 3
        },
        0x12: {
            fn: Instructions.LD_n_A,
            ld: this.indirect_BC,
            cs: 8,
            pc: 1
        },
        0x13: {
            fn: Instructions.INC_DE,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x14: {
            fn: Instructions.INC_D,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x15: {
            fn: Instructions.DEC_D,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x16: {
            fn: Instructions.LD_n_D,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x17: {
            fn: Instructions.RLC_A,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x1A: {
            fn: Instructions.LD_n_A,
            ld: this.indirect_DE,
            cs: 8,
            pc: 1
        },
        0x1B: {
            fn: Instructions.DEC_DE,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x1C: {
            fn: Instructions.INC_E,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x1D: {
            fn: Instructions.DEC_E,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x1E: {
            fn: Instructions.LD_n_E,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x20: {
            fn: Instructions.JR_NZ,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x21: {
            fn: Instructions.LD_i16_HL,
            ld: this.immediate16,
            cs: 12,
            pc: 3
        },
        0x22: {
            fn: Instructions.LDI_HL,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x23: {
            fn: Instructions.INC_HL,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x24: {
            fn: Instructions.INC_H,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x25: {
            fn: Instructions.DEC_H,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x26: {
            fn: Instructions.LD_n_H,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x28: {
            fn: Instructions.JR_Z,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x2B: {
            fn: Instructions.DEC_HL,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x2C: {
            fn: Instructions.INC_L,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x2D: {
            fn: Instructions.DEC_L,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x2E: {
            fn: Instructions.LD_n_L,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x30: {
            fn: Instructions.JR_NC,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x31: {
            fn: Instructions.LD_i16_SP,
            ld: this.immediate16,
            cs: 12,
            pc: 3
        },
        0x32: {
            fn: Instructions.LDD_HL,
            ld: this.register_HL,
            cs: 8,
            pc: 1
        },
        0x33: {
            fn: Instructions.INC_SP,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x35: {
            fn: Instructions.DEC_iHL,
            ld: this.none,
            cs: 12,
            pc: 1
        },
        0x38: {
            fn: Instructions.JR_C,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x3B: {
            fn: Instructions.DEC_SP,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0x3C: {
            fn: Instructions.INC_A,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x3D: {
            fn: Instructions.DEC_A,
            ld: this.none,
            cs: 4,
            pc: 1
        },
        0x3E: {
            fn: Instructions.LD_n_A,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0x47: {
            fn: Instructions.LD_n_A,
            ld: this.register_B,
            cs: 4,
            pc: 1
        },
        0x4F: {
            fn: Instructions.LD_n_A,
            ld: this.register_C,
            cs: 4,
            pc: 1
        },
        0x57: {
            fn: Instructions.LD_n_A,
            ld: this.register_D,
            cs: 4,
            pc: 1
        },
        0x5F: {
            fn: Instructions.LD_n_A,
            ld: this.register_E,
            cs: 4,
            pc: 1
        },
        0x67: {
            fn: Instructions.LD_n_A,
            ld: this.register_H,
            cs: 4,
            pc: 1
        },
        0x6F: {
            fn: Instructions.LD_n_A,
            ld: this.register_L,
            cs: 4,
            pc: 1
        },
        0x77: {
            fn: Instructions.LD_n_A,
            ld: this.indirect_HL,
            cs: 8,
            pc: 1
        },
        0x78: {
            fn: Instructions.LD_n_A,
            ld: this.register_B,
            cs: 4,
            pc: 1
        },
        0x79: {
            fn: Instructions.LD_n_A,
            ld: this.register_C,
            cs: 4,
            pc: 1
        },
        0x7A: {
            fn: Instructions.LD_n_A,
            ld: this.register_D,
            cs: 4,
            pc: 1
        },
        0x7B: {
            fn: Instructions.LD_n_A,
            ld: this.register_E,
            cs: 4,
            pc: 1
        },
        0x7C: {
            fn: Instructions.LD_n_A,
            ld: this.register_H,
            cs: 4,
            pc: 1
        },
        0x7D: {
            fn: Instructions.LD_n_A,
            ld: this.register_L,
            cs: 4,
            pc: 1
        },
        0x7E: {
            fn: Instructions.LD_n_A,
            ld: this.indirect_HL,
            cs: 8,
            pc: 1
        },
        0x7F: {
            fn: Instructions.LD_n_A,
            ld: this.register_A,
            cs: 4,
            pc: 1
        },
        0xA8: {
            fn: Instructions.XOR_r,
            ld: this.register_B,
            cs: 4,
            pc: 1
        },
        0xA9: {
            fn: Instructions.XOR_r,
            ld: this.register_C,
            cs: 4,
            pc: 1
        },
        0xAA: {
            fn: Instructions.XOR_r,
            ld: this.register_D,
            cs: 4,
            pc: 1
        },
        0xAB: {
            fn: Instructions.XOR_r,
            ld: this.register_E,
            cs: 4,
            pc: 1
        },
        0xAC: {
            fn: Instructions.XOR_r,
            ld: this.register_H,
            cs: 4,
            pc: 1
        },
        0xAD: {
            fn: Instructions.XOR_r,
            ld: this.register_L,
            cs: 4,
            pc: 1
        },
        0xAE: {
            fn: Instructions.XOR_r,
            ld: this.indirect_HL,
            cs: 8,
            pc: 1
        },
        0xAF: {
            fn: Instructions.XOR_r,
            ld: this.register_A,
            cs: 4,
            pc: 1
        },
        0xB8: {
            fn: Instructions.CP_n,
            ld: this.register_B,
            cs: 4,
            pc: 1
        },
        0xB9: {
            fn: Instructions.CP_n,
            ld: this.register_C,
            cs: 4,
            pc: 1
        },
        0xBA: {
            fn: Instructions.CP_n,
            ld: this.register_D,
            cs: 4,
            pc: 1
        },
        0xBB: {
            fn: Instructions.CP_n,
            ld: this.register_E,
            cs: 4,
            pc: 1
        },
        0xBC: {
            fn: Instructions.CP_n,
            ld: this.register_H,
            cs: 4,
            pc: 1
        },
        0xBD: {
            fn: Instructions.CP_n,
            ld: this.register_L,
            cs: 4,
            pc: 1
        },
        0xBE: {
            fn: Instructions.CP_n,
            ld: this.indirect_HL,
            cs: 8,
            pc: 1
        },
        0xBF: {
            fn: Instructions.CP_n,
            ld: this.register_A,
            cs: 8,
            pc: 3
        },
        0xC1: {
            fn: Instructions.POP_BC,
            ld: this.none,
            cs: 12,
            pc: 1
        },
        0xC5: {
            fn: Instructions.PUSH_nn,
            ld: this.register_BC,
            cs: 16,
            pc: 1
        },
        0xC9: {
            fn: Instructions.RET,
            ld: this.none,
            cs: 8,
            pc: 0
        },
        0xCB: {
            fn: null,
            ld: this.immediate8,
            cs: null,
            pc: 2
        },
        0xCD: {
            fn: Instructions.CALL_nn,
            ld: this.immediate16,
            cs: 12,
            pc: 0
        },
        0xD1: {
            fn: Instructions.POP_DE,
            ld: this.none,
            cs: 12,
            pc: 1
        },
        0xD5: {
            fn: Instructions.PUSH_nn,
            ld: this.register_DE,
            cs: 16,
            pc: 1
        },
        0xE0: {
            fn: Instructions.LDH_in_A,
            ld: this.immediate8,
            cs: 12,
            pc: 2
        },
        0xE1: {
            fn: Instructions.POP_HL,
            ld: this.none,
            cs: 12,
            pc: 1
        },
        0xE2: {
            fn: Instructions.LD_iC_A,
            ld: this.none,
            cs: 8,
            pc: 1
        },
        0xE5: {
            fn: Instructions.PUSH_nn,
            ld: this.register_HL,
            cs: 16,
            pc: 1
        },
        0xEA: {
            fn: Instructions.LD_n_A,
            ld: this.indirect16,
            cs: 16,
            pc: 3
        },
        0xEE: {
            fn: Instructions.XOR_r,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },
        0xF0: {
            fn: Instructions.LDH_A_in,
            ld: this.immediate8,
            cs: 12,
            pc: 2
        },
        0xF1: {
            fn: Instructions.POP_AF,
            ld: this.none,
            cs: 12,
            pc: 1
        },
        0xF5: {
            fn: Instructions.PUSH_nn,
            ld: this.register_AF,
            cs: 16,
            pc: 1
        },
        0xFA: {
            fn: Instructions.LD_n_A,
            ld: this.indirect16,
            cs: 16,
            pc: 3
        },
        0xFE: {
            fn: Instructions.CP_n,
            ld: this.immediate8,
            cs: 8,
            pc: 2
        },

    };

    private cbInstructions = {
        0x11: {
            fn: Instructions.CB_RLC,
            bit: null,
            reg: 'C',
            cs: 8,
        },
        0x7C: {
            fn: Instructions.CB_BIT,
            bit: 7,
            reg: 'H',
            cs: 8,
        }
    };

}