///<reference path="Cpu.ts"/>
/**
 * Created by michiel on 11-7-15.
 * snippets taken from JSGB. See copyright below.
 *
 * jsgb.lcd.js v0.02 - LCD controller emulation for JSGB, a JS GameBoy Emulator
 * Copyright (C) 2009 Pedro Ladaria <Sonic1980 at Gmail dot com>
 */

class Gpu {
    private cpu: Cpu;
    private canvObj: HTMLCanvasElement;
    private LCDcontext: CanvasRenderingContext2D;

    private frameBuffer: number[];

    private colors = [[0xEF,0xFF,0xDE],[0xAD,0xD7,0x94],[0x52,0x92,0x73],[0x18,0x34,0x42]];

    private LCDImage: ImageData; // LCD canvas image
    private LCDImageData: number[]; // LCD canvas image data

    constructor(cpu: Cpu, canvasObj: any) {
        this.cpu = cpu;
        this.canvObj = canvasObj;
        this.LCDcontext = canvasObj.getContext('2d');

        //this.LCDcontext.width = 160;
        //this.LCDcontext.height = 144;
        this.LCDcontext.fillStyle = 'rgb('+this.colors[0][0]+','+this.colors[0][1]+','+this.colors[0][2]+')';
        this.LCDcontext.fillRect(0,0,160,144);

        this.LCDImage = this.LCDcontext.getImageData(0,0,160,144);
        this.LCDImageData = this.LCDImage.data;
    }

    private gb_Framebuffer_to_LCD() {
        var x = 92160; // 144*160*4
        var y:number[];
        var i = 23040; // 144*160
        while (i) {
            y = this.colors[this.frameBuffer[--i]];
            this.LCDImageData[x-=2] = y[2]; // b
            this.LCDImageData[--x ] = y[1]; // g
            this.LCDImageData[--x ] = y[0]; // r
            y = this.colors[this.frameBuffer[--i]];
            this.LCDImageData[x-=2] = y[2]; // b
            this.LCDImageData[--x ] = y[1]; // g
            this.LCDImageData[--x ] = y[0]; // r
            y = this.colors[this.frameBuffer[--i]];
            this.LCDImageData[x-=2] = y[2]; // b
            this.LCDImageData[--x ] = y[1]; // g
            this.LCDImageData[--x ] = y[0]; // r
            y = this.colors[this.frameBuffer[--i]];
            this.LCDImageData[x-=2] = y[2]; // b
            this.LCDImageData[--x ] = y[1]; // g
            this.LCDImageData[--x ] = y[0]; // r
        }
        this.LCDcontext.putImageData(this.LCDImage, 0,0);
    }
}