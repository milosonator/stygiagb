/**
 * Created by michiel on 11-7-15.
 */
///<reference path="Cpu.ts"/>
///<reference path="Gpu.ts"/>
///<reference path="Sound.ts"/>
///<reference path="Memory.ts"/>
var Emulator = (function () {
    function Emulator() {
        this.memory = new Memory();
        this.cpu = new Cpu();
        this.gpu = new Gpu();
        this.sound = new Sound();
        document.getElementById("emulatorwindow").innerHTML = "Emulator loaded...";
        //todo other init code
    }
    Emulator.prototype.step = function (steps) {
        if (steps === void 0) { steps = 1; }
        //todo take steps steps
    };
    Emulator.prototype.start = function () {
        //todo start emulator
    };
    return Emulator;
})();
var emulator = new Emulator();
//# sourceMappingURL=main.js.map