/**
 * Created by michiel on 11-7-15.
 */
class Memory {
    private mem = new Uint8Array(65536);
    constructor() {
        for(var i = 0; i < this.mem.length; i++){
            this.mem[i] = ~~(Math.random()*255);
        }
    }
    public loadData(addr: number, data: number[]) {
        for (var i = addr; i < data.length; i++) {
            this.mem[i] = data[i];
        }
    }
    public readByte(addr: number) {
        return this.mem[addr];
    }
    public readWord(addr: number) {
        return (this.mem[addr] | (this.mem[addr+1] << 8));
    }
    public writeByte(addr: number, data: number) {
        this.mem[addr] = data;
    }
    public writeWord(addr: number, data: number) {
        this.writeByte(addr, (data & 0x00FF));
        this.writeByte(addr-1, ((data & 0xFF00) >> 8));
    }
}