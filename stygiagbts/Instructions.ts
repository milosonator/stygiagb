///<reference path="Cpu.ts"/>
/**
 * Created by michiel on 13-7-15.
 */
module Instructions {
    export function JR_NZ(cpu: Cpu, args: number[]) {
        if (!cpu.getFlag('Z')) {
            var jumpRelative = (args[0] + 128) % 256 - 128; //make signed
            cpu.PC = (jumpRelative + cpu.PC) & 0x00FF
        }
    }
    export function JR_Z(cpu: Cpu, args: number[]) {
        if (cpu.getFlag('Z')) {
            var jumpRelative = (args[0] + 128) % 256 - 128; //make signed
            cpu.PC = (jumpRelative + cpu.PC) & 0x00FF
        }
    }
    export function JR_NC(cpu: Cpu, args: number[]) {
        if (!cpu.getFlag('C')) {
            var jumpRelative = (args[0] + 128) % 256 - 128; //make signed
            cpu.PC = (jumpRelative + cpu.PC) & 0x00FF
        }
    }
    export function JR_C(cpu: Cpu, args: number[]) {
        if (cpu.getFlag('C')) {
            var jumpRelative = (args[0] + 128) % 256 - 128; //make signed
            cpu.PC = (jumpRelative + cpu.PC) & 0x00FF
        }
    }
    export function RLC_A(cpu: Cpu, args: number[]) {
        //rotate A through carry. reset N, H, set Z if zero, set C to old bit 7 of A
        if (cpu.getFlag('C')) cpu.A = ((cpu.A << 1) | 1) & 0x00FF; // first bit is 1
        else cpu.A = ((cpu.A << 1) & 254) & 0x00FF; // first bit is 0

        cpu.setFlag('N', false);
        cpu.setFlag('H', false);
        cpu.setFlag('C', (128 & cpu.A) == 128);
        //cpu.setFlag('Z', cpu.A == 0); //some docs say unaffected, some say zero, some say affected as normal
        cpu.setFlag('Z', false);
    }
    export function DEC_A(cpu: Cpu, args: number[]) {
        cpu.setFlag('Z', cpu.A == 1); // set/reset zero flag
        cpu.setFlag('H', (cpu.A & 15) > 1); // set H flag if no borrow from bit 4)
        cpu.setFlag('N', true); // set N flag
        cpu.A = (cpu.A - 1) & 0x00FF; // subtract
    }
    export function DEC_B(cpu: Cpu, args: number[]) {
        cpu.setFlag('Z', cpu.B == 1); // set/reset zero flag
        cpu.setFlag('H', (cpu.B & 15) > 1); // set H flag if no borrow from bit 4)
        cpu.setFlag('N', true); // set N flag
        cpu.B = (cpu.B - 1) & 0x00FF; // subtract
    }
    export function DEC_C(cpu: Cpu, args: number[]) {
        cpu.setFlag('Z', cpu.C == 1); // set/reset zero flag
        cpu.setFlag('H', (cpu.C & 15) > 1); // set H flag if no borrow from bit 4)
        cpu.setFlag('N', true); // set N flag
        cpu.D = (cpu.C - 1) & 0x00FF; // subtract
    }
    export function DEC_D(cpu: Cpu, args: number[]) {
        cpu.setFlag('Z', cpu.D == 1); // set/reset zero flag
        cpu.setFlag('H', (cpu.D & 15) > 1); // set H flag if no borrow from bit 4)
        cpu.setFlag('N', true); // set N flag
        cpu.D = (cpu.D - 1) & 0x00FF; // subtract
    }
    export function DEC_E(cpu: Cpu, args: number[]) {
        cpu.setFlag('Z', cpu.E == 1); // set/reset zero flag
        cpu.setFlag('H', (cpu.E & 15) > 1); // set H flag if no borrow from bit 4)
        cpu.setFlag('N', true); // set N flag
        cpu.E = (cpu.E - 1) & 0x00FF; // subtract
    }
    export function DEC_H(cpu: Cpu, args: number[]) {
        cpu.setFlag('Z', cpu.H == 1); // set/reset zero flag
        cpu.setFlag('H', (cpu.H & 15) > 1); // set H flag if no borrow from bit 4)
        cpu.setFlag('N', true); // set N flag
        cpu.H = (cpu.H - 1) & 0x00FF; // subtract
    }
    export function DEC_L(cpu: Cpu, args: number[]) {
        cpu.setFlag('Z', cpu.L == 1); // set/reset zero flag
        cpu.setFlag('H', (cpu.L & 15) > 1); // set H flag if no borrow from bit 4)
        cpu.setFlag('N', true); // set N flag
        cpu.L = (cpu.L - 1) & 0x00FF; // subtract
    }
    export function DEC_iHL(cpu: Cpu, args: number[]) {
        var val = cpu.mem.readByte(cpu.getRegister('HL'));
        cpu.setFlag('Z', val == 1); // set/reset zero flag
        cpu.setFlag('H', (val & 15) > 1); // set H flag if no borrow from bit 4)
        cpu.setFlag('N', true); // set N flag
        cpu.mem.writeByte(cpu.getRegister('HL'), (val - 1) & 0x00FF); //subtract
    }
    export function INC_A(cpu: Cpu, args: number[]) {
        cpu.setFlag('H', (cpu.A & 0x0F) == 0x0F);
        cpu.A = (cpu.A + 1) & 0x00FF;
        cpu.setFlag('Z', cpu.A == 0);
        cpu.setFlag('N', false);
    }
    export function INC_B(cpu: Cpu, args: number[]) {
        cpu.setFlag('H', (cpu.B & 0x0F) == 0x0F);
        cpu.B = (cpu.B + 1) & 0x00FF;
        cpu.setFlag('Z', cpu.B == 0);
        cpu.setFlag('N', false);
    }
    export function INC_C(cpu: Cpu, args: number[]) {
        cpu.setFlag('H', (cpu.C & 0x0F) == 0x0F);
        cpu.C = (cpu.C + 1) & 0x00FF;
        cpu.setFlag('Z', cpu.C == 0);
        cpu.setFlag('N', false);
    }
    export function INC_D(cpu: Cpu, args: number[]) {
        cpu.setFlag('H', (cpu.D & 0x0F) == 0x0F);
        cpu.D = (cpu.D + 1) & 0x00FF;
        cpu.setFlag('Z', cpu.D == 0);
        cpu.setFlag('N', false);
    }
    export function INC_E(cpu: Cpu, args: number[]) {
        cpu.setFlag('H', (cpu.E & 0x0F) == 0x0F);
        cpu.E = (cpu.E + 1) & 0x00FF;
        cpu.setFlag('Z', cpu.E == 0);
        cpu.setFlag('N', false);
    }
    export function INC_H(cpu: Cpu, args: number[]) {
        cpu.setFlag('H', (cpu.H & 0x0F) == 0x0F);
        cpu.H = (cpu.H + 1) & 0x00FF;
        cpu.setFlag('Z', cpu.H == 0);
        cpu.setFlag('N', false);
    }
    export function INC_L(cpu: Cpu, args: number[]) {
        cpu.setFlag('H', (cpu.L & 0x0F) == 0x0F);
        cpu.L = (cpu.L + 1) & 0x00FF;
        cpu.setFlag('Z', cpu.L == 0);
        cpu.setFlag('N', false);
    }
    export function LDH_in_A(cpu: Cpu, args: number[]) {
        cpu.mem.writeByte(0xff00 + args[0], cpu.A);
    }
    export function LDH_A_in(cpu: Cpu, args: number[]) {
        cpu.A = cpu.mem.readByte(0xff00 + args[0]);
    }
    export function LD_iC_A(cpu: Cpu, args: number[]) {
        cpu.mem.writeByte(0xff00 + cpu.C, cpu.A);
    }
    export function LD_n_A(cpu: Cpu, args: number[]) {
        cpu.A = args[0];
    }
    export function LD_n_B(cpu: Cpu, args: number[]) {
        cpu.B = args[0];
    }
    export function LD_n_C(cpu: Cpu, args: number[]) {
        cpu.C = args[0];
    }
    export function LD_n_D(cpu: Cpu, args: number[]) {
        cpu.D = args[0];
    }
    export function LD_n_E(cpu: Cpu, args: number[]) {
        cpu.E = args[0];
    }
    export function LD_n_H(cpu: Cpu, args: number[]) {
        cpu.H = args[0];
    }
    export function LD_n_L(cpu: Cpu, args: number[]) {
        cpu.L = args[0];
    }
    export function LDD_HL(cpu: Cpu, args: number[]) {
        cpu.mem.writeByte(args[0], cpu.A);
        Instructions.DEC_HL(cpu, args);
    }
    export function LDI_HL(cpu: Cpu, args: number[]) {
        cpu.mem.writeByte(args[0], cpu.A);
        Instructions.INC_HL(cpu, args);
    }
    export function INC_BC(cpu: Cpu, args: number[]) {
        cpu.setRegister('BC', (cpu.getRegister('BC') + 1) & 0x00FFFF);
    }
    export function INC_DE(cpu: Cpu, args: number[]) {
        cpu.setRegister('DE', (cpu.getRegister('DE') + 1) & 0x00FFFF);
    }
    export function INC_HL(cpu: Cpu, args: number[]) {
        cpu.setRegister('HL', (cpu.getRegister('HL') + 1) & 0x00FFFF);
    }
    export function INC_SP(cpu: Cpu, args: number[]) {
        cpu.setRegister('SP', (cpu.getRegister('SP') + 1) & 0x00FFFF);
    }
    export function DEC_BC(cpu: Cpu, args: number[]) {
        cpu.setRegister('BC', (cpu.getRegister('BC') - 1) & 0x00FFFF);
    }
    export function DEC_DE(cpu: Cpu, args: number[]) {
        cpu.setRegister('DE', (cpu.getRegister('DE') - 1) & 0x00FFFF);
    }
    export function DEC_HL(cpu: Cpu, args: number[]) {
        cpu.setRegister('HL', (cpu.getRegister('HL') - 1) & 0x00FFFF);
    }
    export function DEC_SP(cpu: Cpu, args: number[]) {
        cpu.setRegister('SP', (cpu.getRegister('SP') - 1) & 0x00FFFF);
    }
    export function LD_i16_BC(cpu: Cpu, args: number[]) {
        cpu.B = (args[0] & 0xff00) >> 8;
        cpu.C = (args[0] & 0x00ff);
    }
    export function LD_i16_DE(cpu: Cpu, args: number[]) {
        cpu.D = (args[0] & 0xff00) >> 8;
        cpu.E = (args[0] & 0x00ff);
    }
    export function LD_i16_HL(cpu: Cpu, args: number[]) {
        cpu.H = (args[0] & 0xff00) >> 8;
        cpu.L = (args[0] & 0x00ff);
    }
    export function LD_i16_SP(cpu: Cpu, args: number[]) {
        cpu.SP = args[0];
    }
    export function XOR_r(cpu: Cpu, args: number[]) {
        cpu.A = cpu.A ^ args[0];
    }
    export function CP_n(cpu: Cpu, args: number[]) {
        cpu.setFlag('C', cpu.A > args[0]); // no borrow: carry set
        cpu.setFlag('Z', cpu.A == args[0]); // if equal then zero
        cpu.setFlag('H', (cpu.A & 0x0F) > (args[0] & 0x0F)); // same as carry but masked
        cpu.setFlag('N', true); //set
    }
    export function CALL_nn(cpu: Cpu, args: number[]) {
        // get addr of next instruction (keep immediate16 in mind)
        // push high part to stack (dec SP, write)
        // push low part to stack (dec SP, write)
        // set PC to immediate16
        var nextInstrAddr = cpu.PC + 3;
        cpu.SP = cpu.SP-1;
        cpu.mem.writeByte(cpu.SP, nextInstrAddr >> 8); // high part (see push)
        cpu.SP = cpu.SP-1;
        cpu.mem.writeByte(cpu.SP, nextInstrAddr & 0x00FF); // low part
        cpu.PC = args[0]
    }
    export function POP_BC(cpu: Cpu, args: number[]) {
        cpu.C = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
        cpu.B = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
    }
    export function POP_DE(cpu: Cpu, args: number[]) {
        cpu.E = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
        cpu.D = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
    }
    export function POP_HL(cpu: Cpu, args: number[]) {
        cpu.L = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
        cpu.H = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
    }
    export function POP_AF(cpu: Cpu, args: number[]) {
        cpu.F = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
        cpu.A = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
    }
    export function PUSH_nn(cpu: Cpu, args: number[]) {
        // push double register onto stack and decrement stack pointer twice
        // for both the push and pull it is the case that the order in which the values are pushed
        // is reversed, so if you look at the memory the pushed value 0x01234 will look like 0x3412
        // take note of the order in which the registers are stored and what registers are accessed
        cpu.SP = cpu.SP-1;
        cpu.mem.writeByte(cpu.SP, args[0] >> 8); // high part (see push)
        cpu.SP = cpu.SP-1;
        cpu.mem.writeByte(cpu.SP, args[0] & 0x00FF); // low part
    }
    var C: number;
    var P: number;
    export function RET(cpu: Cpu, args: number[]) {
        C = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
        P = cpu.mem.readByte(cpu.SP);
        cpu.SP += 1;
        cpu.PC = ( P << 8 ) | C
    }
    export function CB_BIT(cpu: Cpu, bit: number, reg: string) {
        var mask = 0x01 << bit; // eg: bit: 7 mask: 0b10000000
        cpu.setFlag('Z', (cpu.getRegister(reg) & mask) == 0);
        cpu.setFlag('N', false);
        cpu.setFlag('H', true);
    }
    export function CB_RLC(cpu: Cpu, bit: number, reg: string) {
        //rotate A through carry. reset N, H, set Z if zero, set C to old bit 7 of A
        if (cpu.getFlag('C')) cpu.setRegister(reg, ((cpu.getRegister(reg) << 1) | 1) & 0x00FF); // first bit is 1
        else cpu.setRegister(reg, ((cpu.getRegister(reg) << 1) & 254) & 0x00FF); // first bit is 0

        cpu.setFlag('N', false);
        cpu.setFlag('H', false);
        cpu.setFlag('C', (128 & cpu.getRegister(reg)) == 128);
        cpu.setFlag('Z', cpu.getRegister(reg) == 0)
    }
}